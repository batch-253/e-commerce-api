const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

// To connect in user route
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");


const app = express();
const port = process.env.PORT || 4000;
const db = mongoose.connection;

mongoose.connect(
  "mongodb+srv://admin:admin123@batch-253-ferrer.2fpi5nk.mongodb.net/Capstone?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

db.once("open", () => console.log("Now connected to MongoDB Atlas."));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// URI endpoint and to connect
app.use("/users", userRoute);

app.use("/products", productRoute);

if (require.main === module) {
  app.listen(port, () => {
    console.log(`API is now online on http://localhost:${port}`);
  });
}

module.exports = app;
