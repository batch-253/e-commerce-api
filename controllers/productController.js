const Product = require("../model/Product");

const User = require("../model/User")



module.exports.addProduct = (reqBody, isAdmin) => {

	if(isAdmin) {

	let newProduct = new Product ({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price	
		

	});

	return newProduct.save().then(product => true).catch(err => false);

} else {
	return Promise.reject("Not Authorized");
}

};
//======================== END 

//==================== GET ALL PRODUCT WITH JWT ADMIN ==============

module.exports.getAllProduct = () => {
	return Product.find({}).then(result => result)
		.catch(err => err);
};

//======================== END 


//=================== GET ALL ACTIVE PRODUCT ====================

module.exports.getAllActive = () => {

	return Product.find({ isActive : true }).then(result => result)
		.catch(err => err);
};


// ===================== GET SPECIFIC PRODUCT USING ID ====================

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	}).catch(err => err);

};

//======================== END 



//========================= UPDATE PRODUCT USING ADMIN ONLY ========================

module.exports.updateProduct = (reqParams, reqBody) => {


	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
		.then(course => true).catch(err => err);

};

//=========================== END


//=========================== UPDATE ARCHIVE ======================================

module.exports.archiveProduct = (reqParams, reqBody) => {

	let updateSpecificProduct = {
		isActive : reqBody.isActive	
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateSpecificProduct).then(course => true).catch(err => err);
};

//=========================== END