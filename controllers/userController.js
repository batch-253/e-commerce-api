const bcrypt = require("bcrypt");
const auth = require("../auth");

const User = require("../model/User");
const Product = require("../model/Product");

/*
	STRETCH GOALS, CHECK EMAIL DUPLICATE INSIDE REGISTER USER EXPORTS.
	For the meantime separate
*/

//=============================== CHECKEMAIL ==========================
module.exports.checkEmailExists = (reqBody) => {
  return User.find({ email: reqBody.email })
    .then((result) => {
      if (result.length > 0) {
        return true;
      } else {
        return false;
      }
    })
    .catch((err) => err);
};

//=========================== END

//================================= REGISTRATION ==========================

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
  });

  return newUser
    .save()
    .then((user) => {
      if (user) {
        return true;
      } else {
        return false;
      }
    })
    .catch((err) => err);
};

//=========================== END

//================================ LOGIN AUTH =============================

module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then((result) => {
      if (result == null) {
        return false;
      } else {
        const isPasswordCorrect = bcrypt.compareSync(
          reqBody.password,
          result.password
        );
        if (isPasswordCorrect) {
          return { access: auth.createAccesToken(result) };
        } else {
          return false;
        }
      }
    })
    .catch((err) => err);
};

//=========================== END

//============================== NON-ADMIN USER CHECKOUT ============================

module.exports.checkout = (data, body) => {
  const id = data.userId;
  console.log(data);

  

  return User.findById(id).then(user => {
    user.orderedProduct.push({ products : body});
    console.log(user.orderedProduct[0].products);
    return user.save();
  }).catch(err => {
    // handle errors here
    console.error(err);
    throw err;
  });
};




//=========================== END

//====================================== User Details ==========================

module.exports.getProfile = (data) => {
  return User.findById(data.userId).then((result) => {
    result.password = "";

    return result;
  });
};

//================================ END


module.exports.addAdmin = (req, res) => {

    let admin = {isAdmin: res.isAdmin}

    return User.findByIdAndUpdate(req.userId, admin).then(resultFromAdm => true).catch(err => err);
}


