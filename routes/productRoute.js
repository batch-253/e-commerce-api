const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");
const auth = require("../auth");



/*

	STRETCH GOAL EXISTING PRODUCT WILL RESULT DUPLICATE

*/

//====================== ADD PRODUCT ADMIN ONLY====================

router.post("/addProduct", auth.verify, (req, res) => {

	const isAdminData = auth.decode (req.headers.authorization).isAdmin;
	console.log(isAdminData);

	productController.addProduct(req.body, isAdminData).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

//======================== END 





//==================== GET ALL PRODUCT WITH JWT ADMIN ==============

router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.getAllProduct().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});

//======================== END 


//=================== GET ALL ACTIVE PRODUCT =========================

router.get("/", (req,res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

//======================== END 


// ===================== GET SPECIFIC PRODUCT USING ID ====================

router.get("/:productId", (req, res) => {

	console.log(req.params);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

});

//======================= END



//========================= UPDATE PRODUCT USING ADMIN ONLY ========================

router.put("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});

//======================= END

//=========================== UPDATE ARCHIVE ======================================

router.put("/:productId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}
});


//====================== END


module.exports = router;