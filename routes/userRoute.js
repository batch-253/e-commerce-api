const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController")
const auth = require("../auth");


//=============================== CHECKEMAIL ==========================

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});

//=========================== END

//================================= REGISTRATION ==========================
router.post("/register", (req,res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});
//=========================== END

//================================ LOGIN AUTH =============================
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
})

//=========================== END



//============================== NON-ADMIN USER CHECKOUT ============================ 




router.post("/checkout", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId : userData.id,
		isAdmin : userData.isAdmin,

	}
	console.log(data)

	if(!data.isAdmin){

		userController.checkout(data, req.body).then(resultFromController => res.send(resultFromController)).catch(err =>res.send(err));
		console.log(req.body)
	} else {
		res.send(false);
	}
});	


//=========================== END


//====================================== User Details ==========================



router.get("/details", (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({ userId : userData.id }).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});



//================================ END



//====================================


router.put("/:userId", auth.verify, (req, res) => {

    const admin = auth.decode(req.headers.authorization);

    if(admin.isAdmin){

        userController.addAdmin(req.params, req.body).then(resultFromAdmin => res.send(resultFromAdmin)).catch(err => res.send(err))
    } else {
        res.send(false)
    }
})


module.exports = router;
